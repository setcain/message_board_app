In this project add:

Model: https://docs.djangoproject.com/en/2.2/topics/db/models/

Update model data in the built-in admin:
https://docs.djangoproject.com/en/2.2/ref/contrib/admin/

Test: https://docs.djangoproject.com/en/2.2/topics/testing/ 

We've now built, tested, and deployed our first database-driven app.
While it’s deliberately quite basic, now we know how to create a database
model, update it with the admin panel and then display the content on a web
page.

