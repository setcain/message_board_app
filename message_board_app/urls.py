from django.urls import path
from message_board_app.views import HomePageView

urlpatterns = [
    path('', HomePageView.as_view(), name='home_url'),
]
