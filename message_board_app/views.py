from django.views.generic import ListView
from message_board_app.models import Post


class HomePageView(ListView):
    """
    List of Post in template
    """
    model = Post
    template_name = 'home.html'
    context_object_name = 'all_posts_list'
