from django.db import models


class Post(models.Model):
    """
    Create Post model for admin
    """
    text = models.TextField('Mensaje')

    def __str__(self):
        """
        Retrive representative post name in admin
        """
        return self.text[:20]
