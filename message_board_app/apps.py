from django.apps import AppConfig


class MessageBoardAppConfig(AppConfig):
    name = 'message_board_app'
    verbose_name = 'mensajes'
