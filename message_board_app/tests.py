from .models import Post
from django.test import TestCase
from django.urls import reverse


class PostModelTest(TestCase):
    """
    Test post model
    """
    def set_up(self):
        """
        Create message in dummy db
        """
        Post.objects.create(text='This is a test')

        def test_text_content(self):
            """
            Get dummy db text
            """
            post = Post.objects.get(id=1)
            expect_object_name = f'{post.text}'

            self.assertEqual(expect_object_name, 'This is a test')


class HomePageViewTest(TestCase):
    """
    Test HTTP status for homepage
    """
    def set_up(self):
        """
        Create another message in dummy db
        """
        Post.objects.create(text='This is another test')

        def test_view_url_exist_at_proper_location(self):
            """
            Check if status code is 200
            """
            response = self.client.get('/')

            self.assertEqual(response.status_code, 200)

    def test_view_url_by_name(self):
        """
        Check url name
        """
        response = self.client.get(reverse('home_url'))

        self.assertEqual(response.status_code, 200)

    def test_view_users_correct_template(self):
        """
        Check if template is correct
        """
        response = self.client.get(reverse('home_url'))

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'home.html')
